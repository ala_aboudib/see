from seeGraphGrid4 import *
from seeGraphGrid8 import *
from seeGraphRetina import *
from seeImage import *
from seeSystem import *
from seeRFCenterSurround import *
from seeGraphCortex import *
from seeSystem import *
import argparse
import os.path
import numpy as np

def main():


    
    
    #processing command-line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--image", help = "image name with extention")
    args = parser.parse_args()

    imgPath = os.path.dirname(__file__) + "/images/" + args.image

    #sSystem = seeSystem()
    #sSystem.build()
    #sSystem.run(imgPath)
 
    sGraph = seeGraphRetina(visAng = 30,fovAng = 1, fovVtxRad = 100, fovVtxCount = 10000,decFactor = 1.2)
    print sGraph

    #sGraph.draw()



    """
    sImage = seeImage(path = imgPath, diagAng = 20)
    print sImage
    sImage.draw()

    sSystem = seeSystem()
    sSystem.sampleImage(sGraph, sImage)
    print(sGraph.signals)
    sGraph.draw()

    rf = seeRFCenterSurround([1,1], 3, sGraph, True)
    rf.drawRough()
    print cv2.norm(rf.coeffs)

    cortex = seeGraphCortex(sGraph, fovAng = 1, slope = 0.2, radOverlap = 0.3, cirOverlap = 0.3)
    cortex.draw()
    """
  








if __name__ == "__test__":
    main()
