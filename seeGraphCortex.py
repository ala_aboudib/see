from __future__ import division
import math
import numpy as np
import matplotlib.cm as cm
import matplotlib
import scipy.sparse 
from scipy.spatial.distance import euclidean
from seeGraph import *
import sys
from pathos.multiprocessing import Pool
from time import time

class seeGraphCortex(seeGraph):
    """
       this is a class representing any visual layer after the retina such as in 
       LGN, V1, V2,...

       InstanceVariables:
       ------------------
       N
       inGraph
       fovAng
       slope
       RfLayers
       radOverlap
       cirOverlap
       fovRadVtx
       vtxEccent
       vtxRfRad
       radSize
       radCount
       stepAngle
       coords
       signals
       RfInVtxIdx
       RfInVtxDist
       RfWithinImage
       sigTypes
    """


    def __init__(self, inGraph, fovAng, slope, radOverlap, cirOverlap):
        """
           parameters:
           -----------

           :inGraph: seeGraph                
           :fovAng: float
                    the visual angle occupied by the fovea
           :slope: float
                   the inverse magnification factor. this controls the radi of RFs atthis point
           :radOverlap: float
                        overlap between RFs along the same radius.
           :cirOverlap: float
                        overlap between RFs on the same circle.
                        this parameter will be slightly adjusted later in order for all graph radi be equidistant.

           important:
           ----------
           Notice that initially we don't know the number of vertices per radius. this is calculated
           by using fovAng, slope, radOverlap...
        """
        
        self.inGraph = inGraph
        self.fovAng = fovAng
        self.slope = slope
        self.radOverlap = radOverlap
        self.cirOverlap = cirOverlap

        #Here we will start by determining the location of vertices on a radius
        #The vertices are placed so that they satisfy the overlap given by self.radOverlap.
        #In order to understand what the overlap signifies exactly, let's take two circls of
        #radi r1 and r2 where r1 <= r2. defining d as the distance between the centers of the 
        #two circles. olap is the overlap. Then we have the following:
        #olap = 0 => d = r1 + r2
        #olap = 0.5 => d = r2 which means that the bigger circle passes in the center of the smaller one.
        #olap = 1 => d = r2 - r1 which means the the smaller circle touches the bigger one from the inside.
        #in the general case, we have:
        #d = r2 + r1 - olap * 2 * r1      with r2 >= r1

        #within the fovea we consider that all RFs have the same radius
        #this radius is calculated from the RFs at the edge of the fovea. For example, if the radius 
        #of the fovea spans 0.5 degrees and the self.slope (inverse magnification factor) = 0.16 
        #then the radius of all RFs in the fovea would be r = 0.5 * self.slope
      
        #Now we have to compute the eccentricities of vertices in the fovea along a given graph radius
        #first we have to calculate the distance d between two fovea vertices
        #d = r2+ r1 - self.radOverlap * 2 * r1 = 2* r1 *(1-self.radOverlap) since r1 = r2 for all RFs in 
        #the fovea

        fovRfRad = self.slope * (self.fovAng / 2) # the radius of a fovea RF in visual angles
        d = 2 * fovRfRad * (1 - self.radOverlap) #the distance between two foveal vertices
       
        #computing the number of vertices along the fovea radius (integer)
        self.fovRadVtx = (self.fovAng / 2) // d

        #begin to populate the self.vtxEccent variable with the vertex eccentricities on the fovea radius
        #and RfRad with the vertex radi in visual angle along the fovea radius
        self.vtxEccent = []
        RfRad = [] #this is not an instance variable, because later I will define self.vtxRfRad to hold 
                   #radi for each vertex in the graph
        for i in np.arange(self.fovRadVtx):
            self.vtxEccent.append( (i + 1) * d)
            RfRad.append(fovRfRad)

        #important: the eccentricity of the last vertex on the fovea radius is not necessarily equal
        #to the (self.fovAng/2). This is because self.fovRadVtx is forced to be an integer

        #now I have to compute eccentricities of remaining vertices along the graph radius
        #I want the distance d between two vertices whose eccentricities x2 > x1 be so that their
        #radi r2>r1 satisfy the self.radOverlap. we have r2 = self.slope * x2.
        #d = r2 + r1 - self.radOverlap * 2 * r1
        #=>  x2-x1 = r2 + r1 - self.radOverlap * 2 * r1 => ... =>
        #x2 = ( (1 + self.slope - 2 * self.slope * self.radOverlap ) * (x1 / (1 - self.slope)) )

        x1 = self.vtxEccent[-1] #last computed eccentricity along the graph radius
        x2 = ( (1 + self.slope - 2 * self.slope * self.radOverlap ) * (x1 / (1 - self.slope)) ) #the new eccentricity

        #the new calculated eccentricity should fall within the visual field
        while x2 <= (inGraph.visAng / 2): 
            self.vtxEccent.append(x2)
            RfRad.append(x2 * self.slope)
            x1 = x2
            x2 = ( (1 + self.slope - 2 * self.slope * self.radOverlap ) * (x1 / (1 - self.slope)) )

        #convert from list to a numpy array
        self.vtxEccent = np.array(self.vtxEccent)

        #number of vertices per graph radius
        self.radSize = len(self.vtxEccent)

        #now I have to compute the angle (in degrees) between every two adjacent graph radi.
        #This angle is computed so that the circular overlap self.cirOverlap is nearly satisfied.
        #By nearly satisfied, I am referring to the fact that self.cirOverlap will sometimes be
        #adjusted in order for all graph radi be equidistant.

        #Since all vertices on the same circle have the same eccentricity and thus the same RF radius (r),
        #we can define the distance between two adjacent RFs as:
        #
        #d = 2r(1-self.cirOverlap)
        #
        #the angle between the two graph radi on which these two adjacent RFs exist can then be given:
        #sin(self.stepAngle / 2) = d / (2x) = self.slope (1 - self.cirOverlap) where x is the eccentricity
        #=> self.stepAngle = 2 arcsin[self.slope (1 - self.cirOverlap)]

        #initial value of the angle between two adjacent graph radi
        self.stepAngle = 2 * np.arcsin(self.slope * (1 - self.cirOverlap)) #radians

        #adjust self.stepAngle so that all graph radi be equi-distant
        self.radCount = (2 * np.pi) // self.stepAngle   #number of graph radi = number of times self.stepAngle can fit in 2*pi

        #adjust self.stepAngle
        self.stepAngle += ( (2 * np.pi) % self.stepAngle ) / self.radCount

        #Now, since self.stepAngle is adjusted, the circular overlap between two RFs is no more exactly
        #self.cirOverlap. So, we have to compute this new value
        #We have:
        #sin(self.stepAngle / 2) = d / (2x) = self.slope (1 - self.cirOverlap)
        #=> d = 2x * sin(self.stepAngle / 2) = 2x(1-self.cirOverlap) * self.slope
        #=> 

        self.cirOverlap = 1 - (np.sin(self.stepAngle/2)/self.slope)

        #we can now compute the total number of vertices in this graph
        self.N = int( self.radCount * self.radSize + 1 ) # +1 for the center vertex

        #Now we are ready to compute the coordinates of vertices
        self.coords = np.empty((self.N,2) , dtype = np.float32)

        #an array that hold the radius in visual angle of the RF associated with each vertex
        #this radius is calculated as self.slope  *self.vtxEccent out of the fovea
        #and self.slope * (self.fovAng / 2) for vertices in the fovea
        self.vtxRfRad = np.empty(self.N , dtype = np.float32)

        #center vertex coordinates
        self.coords[0,0] , self.coords[0,1] = 0, 0

        #center vertex radius (which is in the fovea)
        self.vtxRfRad[0] = fovRfRad

        for a in np.arange(self.radCount):
            for r in np.arange(self.radSize):  
                
                #index of the vertex
                #after indexing the center vertex, indexing begins by the horizontal radius to the right and proceeds counter-clockwise

                index_cur = a * self.radSize + r + 1 #adding one is necessary because the index 0 is reserved for the center vertex
 
                #calculate the current angle 
                angle = a * self.stepAngle 
                #eccentricity
                eccent = self.vtxEccent[r]
                
                #convert to cartesian coordinates
                x = np.cos(angle) * eccent
                y = np.sin(angle) * eccent

                self.coords[index_cur,0] = x
                self.coords[index_cur,1] = y

                #vertex RF radius in visual angles
                self.vtxRfRad[index_cur] = RfRad[r]

        #define an empty list of signals to be populated later in self.captureSignal()
        self.signals = []

        #create the self.RfLayers variable. This is a variable the holds a list of Rf layers.
        #Each RfLayer is a list of seeRF objects.
        self.RfLayers = []

        #create the instance variables self.RfInVtxIdx and self.RfInVtxDist
        self._findRfInVtxIndexes()



    def _findRfInVtxIndexes(self):
        """
           This function creates an instance variable called self.RfInVtxIdx and self.RfInVtxDist. self.InVtx is a list of numpy arrays.
           An array at position i in self.inVtxIdx contains vertex indexes in self.inGraph that are seen by the RF
           of vertex i in the current graph.
        """
        
        
        #instantiate self.inVtxIdx
        self.inVtxIdx = list();
		
        #create a pool for multiprocessing
        pool = Pool();
		
        #number of available processes
        proc_count = len(pool._pool)
		
        #two lists that hold the results returned by the different processes
        idx_result = list()
        dist_result = list()
		
        #a list of objects that are returned by pool.apply_async
        p = list()

        t1 = time()		

        #create multiple processes. the number of processes is determined by pool._processes
        for i in np.arange(proc_count):
            
            #start a process
            p.append( pool.apply_async(self._proc_findRfInVtxIndexes, \
                                       args = (\
           				       i, \
					       (self.N * i) // proc_count, \
					       (self.N * (i+1)) // proc_count \
		            		)))

        #get and organize the results return by the different threads
        for i in np.arange(proc_count):
            
            #get the results of the pools
            result = p[i].get()
			
            #get returned partition index
            partIdx = result[0][0]
			
            #if the lists idx_result and dist_result are empty, simply append the returned lists to them
            if len(idx_result) == 0:
                idx_result.append(result[0])
                dist_result.append(result[1])

            #else the return lists should be place in increasing order of partIdx	

            #if the returned partition index is greater than the partition index of the last element
            #of idx_result, append the return list to the end of idx_result. (similar judgement of idx_dist)
            elif partIdx > idx_result[-1][0]:
                idx_result.append(result[0])
                dist_result.append(result[1])	    

            else:
                #idx_result and idx_dist should be iterated to decide on where to insert the return list
                for i in arange(len(idx_result) ):
                    if partIdx < idx_result[i][0]:
                        idx_result.insert(i, result[0])
                        dist_result.insert(i, result[1])

        print time() - t1

        #create the instance variable self.RfInVtxIdx and self.RfInVtxDist
        self.RfInVtxIdx = list()
        self.RfInVtxDist = list()

        #populate these variable from the results in idx_result and dist_result
        for i in np.arange(len(idx_result)):
            self.RfInVtxIdx.extend( idx_result[i][1:] ) #in the second index I begin by 1 and not 0 because I want to ignore partIdx
            self.RfInVtxDist.extend( dist_result[i][1:] )
   

    def _proc_findRfInVtxIndexes(self, partIdx, startVtxIdx, endVtxIdx):
        """
        Parameters:
        -----------
        :partIdx:
        :startVtxIdx:
        :endVtxIdx:
        
        returns:
        --------
        :(inVtxIdx, inVtxDist): tuple
        """

        #define two lists that will hold numpy arrays except for the first element
        #which holds partIdx which is the index of the block of vertices that this function
        #is concerned with.
        #inVtxIdx: each element of this array holds a numpy array of inGraph vertexes that fall
        #fall within the receptive filed of the current graph vertex at the corresponding index 
        #relative to startVtxIdx (after removing the first element partIdx). For example, the 
        #numpy array at index 1 holds vertex indexes in inGraph that fall within the RF of the
        #graph vertex RF at index startVtxIdx.
        #inVtxDist: has the exact same organization as inVtxIdx except that information within
        #its numpy arrays  are distances of inGraph vertex from their corresponding RF centers.
        inVtxIdx = [partIdx]
        inVtxDist = [partIdx]
		
        for i in np.arange(startVtxIdx, endVtxIdx):
	        
            #get the coordinates of the current vertex	and its RF radius		
            center = self.coords[i]
            radius = self.vtxRfRad[i]
			
            #a list of inGraph indexes belonging to the RF of the current graph vertex
            lst_idx = list()
			
            #a list of distances of each inGraph vertex to the RF center of the current graph vertex
            lst_dist = list()
			
            for j in np.arange(self.inGraph.N):

                #before calculating the euclidean distance to know wether the vertex is within
                #the RF or not, you can use this to exculde many points
		if np.abs(self.inGraph.coords[j,0] - center[0] ) <= radius and \
                   np.abs(self.inGraph.coords[j,1] - center[1] ) <= radius:

                    #distance in visual angles (since we are using inGraph.coords)
                    d = euclidean(center, self.inGraph.coords[j])

                    if d <= radius:
                        lst_idx.append(j)
                        lst_dist.append(d)
		    
            #populate the lists defined and explained at the top of this function
            inVtxIdx.append( np.array(lst_idx, dtype = np.int32) )
            inVtxDist.append( np.array(lst_dist) )

            print "RF " + str(i)
			
        return inVtxIdx, inVtxDist

    """					
    def _findInVtxIndexes_thread(self, partIdx, startVtxIdx, endVtxIdx):
        
        Parameters:
        -----------
        :partIdx:
        :startVtxIdx:
        :endVtxIdx:
        
        returns:
        --------
        :(inVtxIdx, inVtxDist): tuple
        

        #define two lists that will hold numpy arrays except for the first element
        #which holds partIdx which is the index of the block of vertices that this function
        #is concerned with.
        #inVtxIdx: each element of this array holds a numpy array of inGraph vertexes that fall
        #fall within the receptive filed of the current graph vertex at the corresponding index 
        #relative to startVtxIdx (after removing the first element partIdx). For example, the 
        #numpy array at index 1 holds vertex indexes in inGraph that fall within the RF of the
        #graph vertex RF at index startVtxIdx.
        #inVtxDist: has the exact same organization as inVtxIdx except that information within
        #its numpy arrays  are distances of inGraph vertex from their corresponding RF centers.
        inVtxIdx = [partIdx]
        inVtxDist = [partIdx]
		
        for i in np.arange(startVtxIdx, endVtxIdx):
	        
            #get the coordinates of the current vertex	and its RF radius		
            center = self.coords[i]
            radius = self.vtxRfRad[i]
			
            #a list of inGraph indexes belonging to the RF of the current graph vertex
            lst_idx = list()
			
            #a list of distances of each inGraph vertex to the RF center of the current graph vertex
            lst_dist = list()
			
            for j in np.arange(self.inGraph.N):

                #distance in visual angles (since we are using inGraph.coords)
                d = euclidean(center, self.inGraph.coords[j])
				
                if d <= radius:
                    lst_idx.append(j)
                    lst_dist.append(d)
		    
            #populate the lists defined and explained at the top of this function
            inVtxIdx.append( np.array(lst_idx) )
            inVtxDist.append( np.array(lst_dist) )

            print "RF " + str(i)			
        return inVtxIdx, inVtxDist
        """

    def addRfLayer(self, RfLayer):
        """
           this function adds an RF layer to the current graph. That is, it appends a list of RFs
           to the self.RfLayers variable.
       
           Parameters:
           -----------
           :RfLayer: list
                     this is a list of seeRF objects. such as seeRFGabor or seeRFCenterSurround.
        """

        self.RfLayers.append( RfLayer )


    def addSigTypes(self, sigTypes):
        """
           this method creates the self.sigTypes variable as a dictionary

           parameters:
           -----------
           :sigTypes: dictionary
                  this is a python dictionary composed of keys and values.
                  each key refers to one feature map type, the value of that key is the index
                  in self.signals of the signal corresponding to that map.

                  the keys are the following:
                 
                  "bw-on": map resulting from filtering with on-center accromatic DoG kernels.
                  "bw-off": off-center accromatic DoG kernels.
                  "rg-on": on-red-center, off-green-surround DoG kernels.
                  "rg-off": off-red-center, on-green-surround DoG kernels.
                  "gr-on": on-green-center, off-red-surround DoG kernels.
                  "gr-off": off-green-center, on-red-surround DoG kernels.
                  "by-on": on-blue-center, off-yellow-surround DoG kernels.
                  "by-off": off-blue-center, on-yellow-surround DoG kernels.
                  "yb-on": on-yellow-center, off-blue-surround DoG kernels.
                  "yb-off": off-yellow-center, on-blue-surround DoG kernels.
                  "o-0": 0 degree Gabor kernels.
                  "o-45": 45 degree Gabor kernels.
                  "o-90": 90 degree (horizontal) Gabor kernels.
                  "o-135": 135 degree (bottom-left to top-right) Gabor kernels.
        """

        self.sigTypes = sigTypes



    def checkRfWithinImage(self):
        """
           this function creates the instance variable self.RfWithinImage. This is a somewhat artificial variable.
           self.RfWithScene is a boolean array with self.N elements. when an element is false, this mean that the
           corresponding RF either overlaps the image borders or falls entirely outside it.
	"""
       
        #declare and initialize the instance variable
        self.RfWithinImage = np.zeros( self.N, dtype = np.bool )

        for i in np.arange(self.N):

            #get the signal on self.RfInVtxIdx[i]
            RfInSignal = self.inGraph.signals[ self.RfInVtxIdx[i] ]

            #the signal on self.inGraph vertices that are outside seeImage is -1
            if np.min(RfInSignal) >= 0:
                self.RfWithinImage[i] = True


    def captureSignal(self):
        """
           This function populate the self.signals variable be triggerring the RFs in self.RfLayers.

        """

        #create the self.RfwithinImage instance variable to get information about RFs that fall
        #on the border or outside seeImage
        self.checkRfWithinImage()

        #reinitialize the self.signals variable
        self.signals = list()

        #the number of signals to capture is equal to the number of Rf layers in self.RfLayers
        for j in np.arange( len(self.RfLayers) ): 

            #add a new signal as a numpy 1D vector
            self.signals.append(np.empty(self.N, dtype = np.float32))


            print "RF layer [" + str(j) + "]: begin filtering..."

            for i in np.arange(self.N):

                #construct the input signals of the RFs
                self.RfLayers[j][i].captureInput()

                #take the output value of the RF as the signal value on the current vertex
                self.signals[-1][i] =  self.RfLayers[j][i].fire() * self.RfWithinImage[i]

            print "end filtering."


    def draw(self, show = True, drawRF = True, drawInGraph = False):
        """
           parameters:
           -----------
           :show: boolean
                  this is just a flag, when True, the function uses plt.show() to show the figure on the screen.
                  If False, the figure is created but not shown.
           :drawRF: boolean
                    when True, circles around the vertices would be drawn 
                    to represent the RF span according to self.slope
           :drawInGraph: boolean
                         if True, the input graph of this graph is drawn on the same figure

           returns:
           --------
           :fig: object returned by matplotlib.pyplot.figure()
                 this object allows the figure to return and manipulated later in the calling function.

        """

        
        if drawInGraph == True:
            fig = self.inGraph.draw(show = False)
        else:
            fig = plt.figure()

        #if drawRF is True, show the receptive fields of vertices as circles, else show only points representing vertices
        if drawRF == True:
            for idx in np.arange(self.N):  
                fig.gca().add_patch(plt.Circle(self.coords[idx,:], self.vtxRfRad[idx] , fill = False ))
            #matplotlib.rc('axes', fc = None)
            #fig.gca().scatter(self.coords[:,0],self.coords[:,1], s=100*np.pi * (self.vtxRfRad**2))    

        else:

            fig.gca().scatter(self.coords[:,0],self.coords[:,1], s=0.04)    
 
        

        fig.gca().axis('off')
        fig.gca().axis('scaled')

        #show the figure if the 'show' flag is True
        if show == True:
            plt.show()

        return fig




    def drawSignal(self, sigIdx = None, signals = None, show = True):
        """
           this is a function that plots one graph signal in self.signals indexed by sigIdx.
        
           parameters:
           -----------
           :sigIdx: integer
                    the index of a signal in self.signals to plot.
           :signals: of the same shape as self.signals
                     if None, self.signals is used as the signals source, otherwise signals is used.
           :show: boolean
                  this is just a flag, when True, the function uses plt.show() to show the figure on the screen.
                  If False, the figure is created but not shown.

           returns:
           --------
           :fig: object returned by matplotlib.pyplot.figure()
                 this object allows the figure to return and manipulated later in the calling function.
        """

        fig = plt.figure()

        #decide which signals source to use
        if signals == None:
            signal = np.copy(self.signals[sigIdx])

        #the case where multiple signals are passed
        elif len(signals.shape) > 1: 
            signal = np.copy(signals[sigIdx])

        #the case were only one signal is passed
        else:
            signal = signals

        print np.min(signal), np.max(signal)


        #normalize the color between [0,1]
        #signal = (signal - np.min(signal) ) / ( np.max(signal) - np.min(signal) )
        #signal += np.abs(np.min(signal))

        #signal /= np.max(signal)

        if np.min(signal) < 0:
            signal += np.abs(np.min(signal))

        if np.max(signal) > 1:
            signal = (signal - np.min(signal) ) / ( np.max(signal) - np.min(signal) )

        if np.max(signal) < 1:
            signal = signal / np.max(signal)
 
        #for i in np.arange(self.N):
            #fig.gca().add_patch(plt.Circle(self.coords[i,:], 0.02,  fc = [signal[i]] * 3, color = [signal[i]] * 3))

        fig.gca().scatter(self.coords[:,0],self.coords[:,1], s=2, c = signal, linewidths = 0, cmap = cm.Greys_r)

        fig.gca().axis('off')
        fig.gca().axis('scaled')

        #show the figure if the 'show' flag is True
        if show == True:
            plt.show()

        return fig

        

        
