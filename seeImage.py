from __future__ import division
import numpy as np
import matplotlib.image as mpimg
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import math
import os
import cv2

class seeImage:
    """
       this is a custom Image class that encapsulates and image and associates a visual angle with its diagonal

       Instance Variables:
       -------------------
       :image:
       :height:
       :width:
       :compCount: number of image components (3 for RGB and 1 for grayscale)
       :diag: the size in pixels of image diagonal (this is a float, and it is NOT the number of pixels along the image diagonal)
       :diagAng: the visual angle in degrees associated with the image diagonal. (float)
       :dist: the distance (in pixels) between the image and the viewer.

       Methods:
       -------
    """

    def __init__(self, path, diagAng):
        """
            parameters:
            -----------

            :path: string
                   the path to the image.
            :diagAng: float
                      the visual angle in degrees associated with the image diagonal.

        """

        
        self.image = mpimg.imread(path);
        self.diagAng = diagAng 

        #convert pixel values to the range [0,1]
        self.image = np.float32(self.image / 255)

        
        self.height = self.image.shape[0]
        self.width = self.image.shape[1]

        #extract filename of the image
        self.filename = os.path.basename(path)

        if len(self.image.shape) == 2:
            self.image = cv2.cvtColor(self.image,cv2.COLOR_GRAY2RGB)

        #number of image components (3 for RGB and 1 for grayscale)
        if len(self.image.shape) == 3:
            self.compCount = 3

        #the size in pixels of image diagonal (this is a float, and it is NOT the number of pixels
        #along the image diagonal)
        self.diag = math.sqrt(self.height**2 + self.width**2)

        #the distance (in pixels) between the image and the viewer. Calculated using the the visual angle of its diagonal
        self.dist = (self.diag/2) / np.tan( ( (self.diagAng/2) * np.pi)/180 )


    def getPixValue(self, sample_coord ):
        """
           this is a function that returns the value of one pixel in self.image. The coordinate system
           of self.image that this function understands is euclidean centered on the center of self.image.
           the self.image center is (self.height//2, self.width//2)


           parameters:
           -----------
           :sample_coord: is a tuple containing two integer
               :width_coord: the coordinate (in the centered system) along the the width axis (x-axis).
               :height_coord: the coordinate (in the centered system) along the the height axis (y-axis).

           returns:
           --------
           the value of the pixel (single or RGB triplet) at the indicated position. or -1 if the indicated position is out of the image range.

        """

        try:
           #treat both gray-level and RGB image cases

           width_coord, height_coord = sample_coord

           #translation of the euclidean basis in which width_idx and height_idx are described
           #so that the center is in the top-left corner
           height_coord_tr = -1 * height_coord + self.height//2
           width_coord_tr = width_coord + self.width//2

           if height_coord_tr < 0 or width_coord_tr < 0:            #return -1 if the index is out of bounds in the negative direction
               return -1

           return self.image[height_coord_tr, width_coord_tr]


        except IndexError:
           #return -1 if the index is out of bounds
           return -1


    def getPixIdx(self, proj_coord):
        """
           this is a function that returns the index of one pixel in self.image. The coordinate system
           of self.image that this function takes is euclidean centered on the center of self.image.
           And it returns the classical index of the image pixel situated at proj_coord
           the self.image center is (self.height//2, self.width//2)


           parameters:
           -----------
           :proj_coord: is a tuple containing two integer
               :width_coord: the coordinate (in the centered system) along the the width axis (x-axis).
               :height_coord: the coordinate (in the centered system) along the the height axis (y-axis).

           returns:
           --------
           the index of the pixel the indicated position. or -1 if the indicated position is out of the image range.

        """

        try:

           width_coord, height_coord = proj_coord

           #translation of the euclidean basis in which width_idx and height_idx are described
           #so that the center is in the top-left corner
           height_coord_tr = -1 * height_coord + self.height//2
           width_coord_tr = width_coord + self.width//2

           if height_coord_tr < 0 or width_coord_tr < 0:            #return -1 if the index is out of bounds in the negative direction (which is a case that does not raise an IndexError
               return -1

           return int(height_coord_tr), int(width_coord_tr)


        except IndexError:

           #return -1 if the index is out of bounds
           return -1


          
    def drawImage(self, show = True):
        """draw the image

           parameters:
           -----------
           :show: boolean
                  this is just a flag, when True, the function uses plt.show() to show the figure on the screen.
                  If False, the figure is created but not shown.


           """
        fig = plt.figure()

        if self.compCount == 3:
            fig.gca().imshow(self.image)

        if self.compCount == 1:
            fig.gca().imshow(self.image, cmap = cm.Greys_r)
        
        #show the figure if the 'show' flag is True
        if show == True:
            plt.show()   




    def __str__(self):
        
        outputStr = ""

        if hasattr(self, 'image'):
            if self.compCount == 1:
                outputStr +=  "\n\timage: [" + str(self.image.shape[0]) + " x "  + str(self.image.shape[1]) + "] " + str(type(self.image[0,0]))
            if self.compCount == 3:
                outputStr +=  "\n\timage: [" + str(self.image.shape[0]) + " x "  + str(self.image.shape[1]) + " x "  + str(self.image.shape[2]) + "] " + str(type(self.image[0,0,0]))

        if hasattr(self, 'compCount'):
            outputStr += "\n\tcompCount: " + str(self.compCount) + " components"

        if hasattr(self, 'height'):
            outputStr += "\n\theight: " + str(self.height) + " pixels"

        if hasattr(self, 'width'):
            outputStr += "\n\twidth: " + str(self.width) + " pixels"

        if hasattr(self, 'diag'):
            outputStr += "\n\tdiag: " + str(self.diag) + " pixels"

        if hasattr(self, 'diagAng'):
            outputStr += "\n\tdiagAng: " + str(self.diagAng) + " degrees"

        if hasattr(self, 'dist'):
            outputStr += "\n\tdist: " + str(self.dist) + " pixels"
        

        return outputStr
