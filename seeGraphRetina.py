from __future__ import division
import math
import numpy as np
import scipy.sparse 
from scipy.spatial.distance import euclidean
from seeGraph import *
import sys


class seeGraphRetina(seeGraph):
    """
       This class generates a graph inspired by the distribution of cones in the retina.

       Important:
       ----------
       
       all reference to retina radius does not include the center vertex. For example,
       self.radSize is the number of vertices per radius not including the center vertex.

       Instance variables:
       -------------------
       :self.visAng:
       :self.fovAng:
       :self.fovVtxRad:
       :self.decFactor:
       :self.directed:
       :self.type:      
       :self.stepAngle:
       :self.fovVtxCount:
       :self.effectiveDivCount:
       :self.radSize:
       :self.radCount: 
       :self.vtxSpacing:
       :self.vtxEccent:   the eccentricity of each vertex along the the radius in visual angles
       :self.N:
       :self.coords:
       :self.projCoords:
       :self.W:
       :self.A:
       :self.signals:
       :self.focusCenter:

       Methods:
       --------
       :__init__(self, visAng, fovAng, fovVtxRad, fovVtxCount, decFactor):
       :sampleImage(self, sImage):
       :computeProjCoords(self, dist):
       :draw(self):
       :drawProj(self):
       :drawSignal(self):
       :_createAdjMat(self): (Useless)

    """

    def __init__(self, visAng, fovAng, fovVtxRad, fovVtxCount, decFactor):
        """
           parameters:
           -----------

           :visAng: the visual angle occupied by the graph
           :fovAng: the visual angle occupied by the fovea
           :fovVtxRad: the number of vertices along the radius of the fovea
           :fovVtxCount: the approximate total number of vertices desired for the fovea
           :decFactor: the divisive decrease factor in the number of vertices from one visual
                       angle to another along a 1D axis

           Remarks:
           --------

           fovVtxCount is just an approximation, the true number of vertices in the retina might differ.
           fovVtxCount is used first to calculate the step angle of the circular grid. For example
           if fovVtxRad = 20 and fovVtxCount = 170, first we calculate the number of times the fovVtxRad
           should be repeated

           radCount = integer((170 - 1)/20) = 8   where we substract 1 for the center vertex

           then we calculate the step angle: 
    
           stepAngle = 360 / 8 = 45 degrees which is the degree by which to separate radi

             \|/
           --   --
             /|\

           and hence, the true number of vertices in the fovea would be

           fovVtxRad * radCount + 1 = 20 * 8 + 1 = 161 and not 170.


        """

        super(seeGraphRetina, self).__init__()

        self.visAng = visAng
        self.fovAng = fovAng
        self.fovVtxRad = fovVtxRad
        self.decFactor = decFactor

        self.directed = False

        self.type = "Retina Graph"

        #calculating the number of times the fovVtxRad should be circularly repeated
        self.radCount = (fovVtxCount - 1 ) // self.fovVtxRad

        #circular step angle (in degree); angle by which to separate radi
        self.stepAngle = 360 / self.radCount

        #calculating the true number of vertices in the fovea
        self.fovVtxCount = self.fovVtxRad * self.radCount + 1

        #number of half visual angles that can be populated by vertices given the desired decFactor
        #To understand this, let's take an example:
        #visAng = 30, fovAng = 1, fovVtxRad = 20, decFactor = 2
        #so the first  half visual angle which is in the fovea contains 20 vertices
        #second half visual angle contains 20/decfactor = 10
        #third = 5, fourth = 2.5 ~ 3, fifth 2.5/2 = 1.25 ~ 1, sixth = 1.25 / 2 = 0.625 < 0 (so we can consider that no complete vertices really exist).
        #So we have 4 visual angles that can  be populated really plus the fovea
        #to calculate this number quickly we first calculate the number of divisions that give complete elements 
        # effectivDivCount = integer( log(20)/log(decFactor) ) + 1 = 5 ( plus one is for half the fovea)
        #
        #In this case, only the half the fovea and 4 half visual angles of each side of the visual field can be really populated ( a total of 5 out of 30 visual angles available) which is not enought to populate the whole visual field. So the decFactor should be decreased.

        self.effectiveDivCount = math.log(fovVtxRad) // math.log(self.decFactor) + 1

       

        #create an array of vertex number in each division
        #the size of this array is equal number of half visual angle per side of the visual field
        #which is also equal to visAng
        #if effectiveDivCount is lower than visAng, the remaining division will be populated be on vertex each
        divVtxCount = np.ones( self.visAng, dtype = np.int32 )

		#now we fix the iteration range for filling divVtxCount   
        if self.effectiveDivCount >= self.visAng:
            fillRange = self.visAng
        else:
            fillRange = self.effectiveDivCount 

        temp = self.fovVtxRad #start division from this value which is the number of vertices in half of the fovea

        for i in np.arange(fillRange):
            temp = float(temp) / self.decFactor
            divVtxCount[i] = np.round(temp)

        #this is the total number of vertices per radius ((excluding the center vertex))
        self.radSize = np.sum(divVtxCount)


        #create an array of visual angle spacing within each division
        #Example: if we have a division containing 10 vertices, and by considering that this 
        #division spans 1 visual degree, then the spacing between each pair of vertices within this
        #division becomes 1/10 = 0.1 degrees
 
        divSpacing = np.zeros( self.visAng, dtype = np.float32 )

        for i in np.arange(self.visAng):
        	divSpacing[i] = (self.fovAng / 2) / divVtxCount[i]

        #create an array of the spacing in visual angles between each two adjacent
        #vertices along the radius
        self.vtxSpacing = np.zeros( self.radSize, dtype = np.float32 )

        index = 0 #just an index :)
        for i in np.arange(divSpacing.shape[0]):
            for j in np.arange(divVtxCount[i]):
                self.vtxSpacing[index] = divSpacing[i]
                index += 1

        #now calculate the eccentricity of each vertex along the the radius
        self.vtxEccent = np.cumsum(self.vtxSpacing)


        #calculate the number of vertices in the graph 
        #center vertex + sum(divVtxCount) * radCount
        self.N = 1 + self.radSize * self.radCount     


        #declare the coordinate array as euclidean coordinates (the unit of these coordinates are visual angles in degrees)
        self.coords = np.zeros( (self.N, 2), dtype = np.float32)

        #setting the fovea polar coordinates. index 0 for the eccentricity and index 1 for the angle
        #a few lines later, the coordinates would be converted to cartisian. so index 0 will represent then
        #the classical x coordinate on the horizontal axis. and the index 1 will represent the y axis
        self.coords[0,0] = 0
        self.coords[0,1] = 0


        for a in np.arange(self.radCount):
            for r in np.arange(self.radSize):  
                
                #index of the vertex
                #after indexing the center vertex, indexing begins by the horizontal radius to the right and proceeds counter-clockwise

                index_cur = a * self.radSize + r + 1 #adding one is necessary because the index 0 is reserved for the center vertex
 
                #calculate the current angle + some randomness
                angle = a * self.stepAngle + (random.random()-0.5)*self.stepAngle
                #eccentricity + some randomness
                eccent = self.vtxEccent[r] +(random.random()-0.5)*self.vtxSpacing[r]
                
                #convert to cartesian coordinates
                x = np.cos((angle*np.pi)/180) * eccent
                y = np.sin((angle*np.pi)/180) * eccent

                self.coords[index_cur,0] = x
                self.coords[index_cur,1] = y

        """
        #weight matrix (in visual angles) 
        #only elements on a given radius are pair-wize connected for the moment
        self.W = scipy.sparse.lil_matrix((self.N, self.N), dtype = np.float32)

        for a in np.arange(self.radCount):

            #weight between the center vertex and the first vertex of the radius a
            self.W[0, a * self.radSize + 1] = euclidean(self.coords[0,:], self.coords[a * self.radSize + 1,:])

            #for symmetry in the weight matrix
            self.W[a * self.radSize + 1, 0] = self.W[0, a * self.radSize + 1]

            for r in np.arange(self.radSize):

                index_cur = a * self.radSize + r + 1
                
                #distance between a vertex and the preceding vertex
                if r > 0:
                    self.W[index_cur, index_cur - 1] = self.W[index_cur - 1, index_cur] 
                
                #euclidean distance between a vertex and the vertex next to it along the radius
                if r < self.radSize - 1:
                    self.W[index_cur, index_cur + 1] = euclidean(self.coords[index_cur,:], self.coords[index_cur + 1,:])




        #creating the adjacency matrix of boolean type
        self.A = self.W != 0.0
        #self._createAdjMat()

        """

 
          

    def sampleImage(self, sImage, focusCenter = (0,0) ):
        """
           A function that samples a seeImage using the graph vertices

           parameters:
           -----------

           :sImage: seeImage
                    2D image to sample
           :focusCenter: tuple
                         a tuple of the form (x,y) where x and y are the coordinates of the center of 
                         gaze on sImage in (visual angles)

           returns:
           --------

           :self.signals: a reference to the instance variable self.signals. This is a vector with
                           self.N elements . each element of self.signals is a vector representing
                           the value of one pixel sImage. (R, G, B) for colored images or one scalar
                           for grayscale images. 
                           All vertices outside of sImage get the value -1 in self.signals

        """


        self.focusCenter = focusCenter
        
        #create the instance variable self.projCoords, which computes the vertices positions on the image
        self.projCoords = self.computeProjCoords(sImage.dist, self.coords)

        #convert the focus of gaze coordinates into projection coordinates
        focusCenterProj = self.computeProjCoords(sImage.dist, focusCenter)

        #create the instance variable self.signal and initialize its values to -1
        self.signals = -1 * np.ones( (self.N, sImage.compCount), dtype = np.float32) 

        #sample the image
        for i in np.arange(self.N):
                sample_coord = self.projCoords[i] + focusCenterProj #translation to the center of gaze
                self.signals[i,:] = sImage.getPixValue(sample_coord)









    def draw(self, show = True):
        """plots the graph in 2D

           parameters:
           -----------
           :show: boolean
                  this is just a flag, when True, the function uses plt.show() to show the figure on the screen.
                  If False, the figure is created but not shown.

           returns:
           --------
           :fig: object returned by matplotlib.pyplot.figure()
                 this object allows the figure to return and manipulated later in the calling function.
        """


        fig = plt.figure()

        fig.gca().scatter(self.coords[:,0],self.coords[:,1], s=0.01)    
        

        fig.gca().axis('off')
        fig.gca().axis('scaled')

        #show the figure if the 'show' flag is True
        if show == True:
            plt.show()

        return fig




    def drawProj(self):
        """
           plots the projection of the graph on a flat surface. 
           (according to the coordinated in self.projCoords)

           parameters:
           -----------
           :show: boolean
                  this is just a flag, when True, the function uses plt.show() to show the figure on the screen.
                  If False, the figure is created but not shown.

           returns:
           --------
           :fig: object returned by matplotlib.pyplot.figure()
                 this object allows the figure to return and manipulated later in the calling function.
        """

        try:
            fig = plt.figure()

            signals = np.where(self.signals < 0, 0, self.signals)

            fig.gca().scatter(self.projCoords[:,0],self.projCoords[:,1], s=0.8, c = signals, linewidths = 0)
        
            fig.gca().axis('off')
            fig.gca().axis('scaled')

            #show the figure if the 'show' flag is True
            if show == True:
                plt.show()

        except AttributeError, ae:

            print(ae)
            print("You need to execute the function seeGraphRetina.computeProjCoords(self, dist) \
                   in order to generate projection coordinates seeGraphRetina.projCoords")


        return fig



    def drawSignal(self, show = True):
        """
           this is a function that draws the self.signals as an image.

           parameters:
           -----------
           :show: boolean
                  this is just a flag, when True, the function uses plt.show() to show the figure on the screen.
                  If False, the figure is created but not shown.

           returns:
           --------
           :fig: object returned by matplotlib.pyplot.figure()
                 this object allows the figure to return and manipulated later in the calling function.
        """

        fig = plt.figure()

        signals = np.where(self.signals < 0, 0, self.signals)

        fig.gca().scatter(self.coords[:,0],self.coords[:,1], s=0.8, c = signals, linewidths = 0)

        
        fig.gca().axis('off')
        fig.gca().axis('scaled')

        #show the figure if the 'show' flag is True
        if show == True:
            plt.show()

        return fig



    def _createAdjMat(self):
        """
           this function create the adjacency matrix instance variable self.A, it is called by
           __init__().
        """
    
        self.A = scipy.sparse.lil_matrix((self.N, self.N), dtype = np.bool)

        #connecting the center vertex to the first vertex of each radius
        for a in np.arange(self.radCount):

            index = a * self.radSize + 1 #adding one is necessary because the index 0 is reserved for the center vertex
            self.A[0,index] = True
            self.A[index,0] = True


        #connecting each vertex to its 8-neighbors in the same manner as in 8-connected grid
        #we can just consider this as a circular grid where radi represent rows and circles
        #represent columns 
        for a in np.arange(self.radCount):
            for r in np.arange(self.radSize): 
  
                idx_cur = a * self.radSize + r + 1 #adding one is necessary because the index 0 is reserved for the center vertex
                
                idx_e = a * self.radSize + (r+1) + 1 #the index of the next vertex belonging to the same radius but further from the center (circular east)
                idx_w = a * self.radSize + (r-1) + 1 #the index of the previous vertex belonging to the same radius but nearer the center (circular west)
                idx_n = ((a+1) % self.radCount) * self.radSize + r + 1 #the index of the vertex belonging to the same circle but in the next radius (circular north). next redius is in the counter clockwise direction
                idx_s = ((a-1) % self.radCount)* self.radSize + r + 1 #the index of the vertex belonging to the same circle but in the previous radius (circular south)

                idx_ne = ((a+1) % self.radCount) * self.radSize + (r+1) + 1 #north-east vertex
                idx_nw = ((a+1) % self.radCount) * self.radSize + (r-1) + 1 #north-west vertex
                idx_se = ((a-1) % self.radCount) * self.radSize + (r+1) + 1 #south_east vertex
                idx_sw = ((a-1) % self.radCount) * self.radSize + (r-1) + 1 #south_west vertex


                #begin connecting
                self.A[idx_cur, idx_n] = True
                self.A[idx_cur, idx_s] = True

                if r < (self.radSize - 1):
                    self.A[idx_cur, idx_e] = True

                if r > 0:
                    self.A[idx_cur, idx_w] = True

                if r < (self.radSize - 1):
                    self.A[idx_cur, idx_ne] = True

                if r > 0:
                    self.A[idx_cur, idx_nw] = True

                if r < (self.radSize - 1):
                    self.A[idx_cur, idx_se] = True

                if r > 0:
                    self.A[idx_cur, idx_sw] = True




