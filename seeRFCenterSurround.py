from __future__ import division
import math
import numpy as np
import matplotlib.cm as cm
import scipy.sparse 
from scipy.spatial.distance import euclidean
from seeGraph import *
from seeRF import *
import sys
import cv2

class seeRFCenterSurround(seeRF):
    """
       this class represents a center-surround receptive field RF

       Instance variables:
       -------------------
       :N: 
       :center:
       :radius:
       :centerOn:
       :inVtxIdx:
       :isVtxInCenter:
       :coeffs:
       :inSignal:
       :inGraph:
       :inSignalInCenter:
       :inSignalInSurround:
    """

    def __init__(self, center, radius, inVtxIdx, inVtxDist, inGraph, centerOn, inSignalInCenter, inSignalInSurround):
        """
           parameters:
           -----------
           :center: array_like
                    the (x,y) euclidean coordinates of the RF.
           :radius: float
                    the radius of the RF in visual angles.
           :inVtxIdx: array_like (numpy)
                      an array that holds vertex indexes in inGraph that are inputs to the current RF              
           :inVtxDist: array_like (numpy)
                      an array that holds the distance of each input vertex in inVtxIdx to the center
                      of the RF (in visual angles).
           :inGraph: seeGraph   
                     the graph on which to apply the RF.
           :CenterOn: boolean
                      determines wether the RF is On-center or Off-center
           :inSignalInCenter: tuple of booleans
                              this tuple has the same size as inGraph.signals.shape[1] 
                              which is the number of signals living on inGraph.
                              when True, the corresponding signal is seen by the center of the RF. 
           :inSignalInSurround: tuple of booleans
                                this tuple has the same size as inGraph.signals.shape[1] 
                                which is the number of signals living on inGraph.
                                when True, the corresponding signal is seen by the surround of the RF. 
           
        """
     
        self.radius = radius
        self.center = center
        self.inVtxIdx = inVtxIdx
        self.inVtxDist = inVtxDist
        self.centerOn = centerOn
        self.inGraph = inGraph
        self.inSignalInCenter = inSignalInCenter
        self.inSignalInSurround = inSignalInSurround
        
        #Number of input vertices
        self.N = len( self.inVtxIdx )

        #a list that determines for each input vertex wether it is a center vertex (True) or not (False)
        self.isVtxInCenter = []

        #the filter coefficients at this vertex
        self.coeffs = []

        #calculate the RF coefficients at the input vertex positions
        for d in  self.inVtxDist: 
            
            #get RF coefficient value at this vertex
            coeff = self.getCoeff(d)

            #add value to the self.coeffs list
            self.coeffs.append(coeff)

            #determine wether the discovered vertex is a center of a surround vertex
            if coeff >= 0 and self.centerOn == True:
                self.isVtxInCenter.append(True)
            else:
                self.isVtxInCenter.append(False)

        #the following division might cause a division by zero when the RF does not find
        #vertices in its neighborhood
        #in this case
        if self.N != 0:

            #force the integral of the filter coefficients to be zeros
            self.coeffs = self.coeffs - np.sum(self.coeffs) / self.N

            #L2-normalize the vector of coefficients
            self.coeffs = cv2.normalize(self.coeffs)

            #reshape into 1D array
            self.coeffs = self.coeffs.reshape( len(self.coeffs) )


        #convert the list into numpy arrays and reshape it into 1D arrays
        self.isVtxInCenter = np.array(self.isVtxInCenter).reshape( len(self.isVtxInCenter) )


    def captureInput(self):
        """
           this function captures its input signal from inGraph.signals
           this function create the instance variable self.inSignal


          remarks:
          --------
          It is possible that a signal be seen by the center and the surround at the same time.
          For example, if inSignalInCenter = (True, True,True), inSignalInSurround = (True,True,True),
          This means that both the center and the surround of the RF consider all signals. This is equivalent
          to a parasol ganglion cell which extracts intensity contrast information and not color info.


        """

        #take the inGraphsignal values at self.inVtxIdx
        signal = self.inGraph.signals[self.inVtxIdx]

        #initialize the input signal of the seeRFCenterSurroud
        self.inSignal = np.zeros(self.N, dtype = np.float32);

        #extract the center input signal
        self.inSignal = (np.sum(signal * self.inSignalInCenter, axis = 1) / np.sum(self.inSignalInCenter)) * self.isVtxInCenter

        #extract the surround input signal
        isVtxInSurround = np.logical_not(self.isVtxInCenter)

        self.inSignal = self.inSignal + (np.sum(signal * self.inSignalInSurround, axis = 1) / np.sum(self.inSignalInSurround)) * isVtxInSurround




    def fire(self):
        """
           this function performs an inner product between its coefficients 
           and its input signal (self.coeffs * self.inSignal) and returns the results.

           returns:
           --------
           the result of the inner product between self.coeffs and self.inSignal
        """

        
        return np.inner(self.coeffs, cv2.normalize(self.inSignal).reshape(len(self.coeffs)))


    def getCoeff(self, dist):
        """
           this function implements the equation that computes the RF filter coefficient 
           at a point at a given distance from the center
 
           parameters:
           -----------
           :dist: distance of the vertex from self.center

           returns:
           --------
           the coefficient value at 'dist'
        """

        #in Rodiek's paper 1965 (quantitative analysis of cat....) page591,
        #it says that the equation parameter's presented are presented for an RF
        #of diameter of diameter 5 degrees, so the following 2 lines of codes
        #converts the parameter :dist: given into a corresponding distance in an RF
        #of diameter 5 
        refRadius = 5 #5 degrees as in Rodieck1965b
        d = (dist / (self.radius * 2) ) * refRadius

        #setting the parameters of the center-surround kernel as in 
        gamma = 0.8 #gamma = g2 / g1    ,according to Rodieck1965
        g1 = 1
        g2 = g1 * gamma

        beta = 3 #beta = sig2 / sig1    ,according to Rodieck1965
        sig1 = 0.59 #degrees
        sig2 = sig1 * beta

        return (2 * self.centerOn - 1) * \
               ( g1 * sig1**(-2) * np.pi**(-1) * np.exp((-d**2)/sig1**(2)) - \
                 g2 * sig2**(-2) * np.pi**(-1) * np.exp((-d**2)/sig2**(2)) )


    def drawRough(self):
        """
           this is a function that roughly plots the RF on a 2D plane
           'may be deleted later'


           parameters:
           -----------
           :show: boolean
                  this is just a flag, when True, the function uses plt.show() to show the figure on the screen.
                  If False, the figure is created but not shown.

           
           important:
           ----------
           due to poor implementation, the RF should by entirely located in a plane
           quarter in order to be plotted


        """
        coords = np.empty((len(self.inVtxIdx),2), dtype = np.int32)

        for i in np.arange(len(self.inVtxIdx)):
            coords[i,:] = (self.inGraph.projCoords[self.inVtxIdx[i],:])

        mxH = np.max(coords[:,0])
        mnH = np.min(coords[:,0])
        mxW = np.max(coords[:,1])
        mnW = np.min(coords[:,1])

        kernel = np.zeros((np.abs(mxH - mnH) , np.abs(mxW - mnW) ), dtype = np.float32)

        for i in np.arange(len(self.coeffs)):
            kernel[np.abs(coords[i,0] - mnH -1), np.abs(coords[i,1] - mnW - 1)] = self.coeffs[i]




        plt.imshow(kernel, interpolation = 'nearest')
        plt.colorbar()

        plt.show()



    def draw(self, show = True):
        """
           
           parameters:
           -----------
           :show: boolean
                  this is just a flag, when True, the function uses plt.show() to show the figure on the screen.
                  If False, the figure is created but not shown.

           returns:
           --------
           :fig: object returned by matplotlib.pyplot.figure()
                 this object allows the figure to return and manipulated later in the calling function.
        """

        fig = plt.figure()

        #array of vertex colors
        vcolor = np.copy(self.coeffs)
 
        #normalize the values between 0 and 1
        vcolor = (vcolor - np.min(vcolor) ) / (np.max(vcolor) - np.min(vcolor) )

        for i in np.arange(self.N):
            fig.gca().add_patch(plt.Circle(self.inGraph.coords[self.inVtxIdx[i],:], 0.0005, fc = [vcolor[i]]*3, color = [vcolor[i]]*3))


        fig.gca().axis('off')
        fig.gca().axis('scaled')

        #show the figure if the 'show' flag is True
        if show == True:
            plt.show()

        return fig




    def __str__(self):
        pass

