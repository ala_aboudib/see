from seeGraphRetina import *
from seeImage import *
from seeSystem import *
from seeRFCenterSurround import *
from seeGraphCortex import *
from seeSystem import *
import argparse
import os.path
import numpy as np

def main():
   
    
    #processing command-line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--image", help = "image name with extention")
    args = parser.parse_args()

    imgPath = os.path.join(os.path.dirname(__file__), "temp", args.image )

    sSystem = seeSystem()
    #sSystem.build()
 
    sSystem.load()

    sImage = seeImage(path = imgPath, diagAng = 10)

    sSystem.sGraphRetina.sampleImage(sImage = sImage) 
    sSystem.sGraphRetina.drawSignal(show = False);
    sSystem.sGraphCortex.draw(show = False, drawRF=True)
    sSystem.sGraphCortex.captureSignal()
    sSystem.sGraphCortex.drawSignal(sigIdx = 4, show = False)
    plt.show()
    """
    #sGraph.show()




    print sImage
    #sImage.show()

    sSystem = seeSystem()
    sSystem.sampleImage(sGraph, sImage)
    print(sGraph.signals)
    sGraph.show()

    rf = seeRFCenterSurround([1,1], 3, sGraph, True)
    rf.showRough()
    print cv2.norm(rf.coeffs)

    cortex = seeGraphCortex(sGraph, fovAng = 1, slope = 0.2, radOverlap = 0.3, cirOverlap = 0.3)
    cortex.show()
    """








if __name__ == "__main__":
    main()
