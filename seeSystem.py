from __future__ import division
import math
import numpy as np
import scipy.sparse 
from scipy.spatial.distance import euclidean
from seeGraph import *
import sys
from seeGraphRetina import *
from seeImage import *
from seeRFCenterSurround import *
from seeRFGabor import *
from seeGraphCortex import *
from seeSaliency import *
import cPickle
import os
from pathos.multiprocessing import Pool
import shutil

class seeSystem:
    """
       A class to represent the SEE visual system

       Member Variables:
       -----------------
       :sGraphRetina:
       :sGraphCortex:
       :dataPath:
    """

    def __init__(self):
        self.dataPath = os.path.join(os.path.dirname(__file__), "data" )


    def build(self):
        """
          this function create the seeSystem components (seeGraphRetina, seeGraphCortex,...)
          and picle them as files in the 'data' directory
        """

        print "!!! Building seeSystem... !!!"

        #if self.dataPath folder already exists, delete it
        if os.path.exists(self.dataPath):
            shutil.rmtree(self.dataPath)

        #create the self.dataPath directory
        os.mkdir(self.dataPath)

        ###fileOp: Create the pickle file
        f_seeSystem = open( os.path.join(self.dataPath, "seeSystem") ,'wb')
        pickler = cPickle.Pickler(f_seeSystem)
        ###

        print "Creating sGraphRetina.."
        sGraphRetina = seeGraphRetina(visAng = 10,fovAng = 1, fovVtxRad = 100, fovVtxCount = 10000,decFactor = 1.2)
        print sGraphRetina
        print "sGraphRetina is created."



        print "Creating sGraphCortex.."
        sGraphCortex = seeGraphCortex(sGraphRetina, fovAng = 1, slope = 0.16, radOverlap = 0.8, cirOverlap = 0.8)
        print "sGraphCortex is created."


        #now create a list of seeRFCenterSurround configurations for center and surround
        #elements in lst_inSignalInCenter and lst_inSignalInSurround at the same index are
        #sent to the same RF. For example, all seeRFCenterSurround that are responsible of
        #detecting contours are sent lst_inSignalInCenter[i] = (True,True,True),
        #lst_inSignalInSurround[i] = (True,True,True).

        lst_inSignalInCenter = [ (True,True,True) ,
                                 (True,False,False),
                                 (False,True,False),
                                 (True,True,False),
                                 (False,False,True) ]        

        lst_inSignalInSurround = [ (True,True,True) ,
                                   (False,True,False),
                                   (True,False,False),
                                   (False,False,True),
                                   (True,True,False) ]



        for j in np.arange(len(lst_inSignalInCenter) ):

            #declare a RF layer
            lst_RF = []

            for i in np.arange(sGraphCortex.N):
                lst_RF.append (seeRFCenterSurround(center = sGraphCortex.coords[i,:], \
                                                   radius = sGraphCortex.vtxRfRad[i], \
                                                   inVtxIdx = sGraphCortex.RfInVtxIdx[i], \
                                                   inVtxDist = sGraphCortex.RfInVtxDist[i], \
                                                   inGraph = sGraphRetina, \
                                                   centerOn = True, \
                                                   inSignalInCenter = lst_inSignalInCenter[j], \
                                                   inSignalInSurround = lst_inSignalInSurround[j], \
                                                   ))


                print i


            #add the RF list (or layer) to sGraphCortex
            sGraphCortex.addRfLayer(lst_RF)

        #Add gabor Rf Layers
        thetas = [0, 45, 90, 135] #a list of angles (in degrees) to be used in defining Gabor kernels

        for j in np.arange(len(thetas)):

            #declare a RF layer
            lst_RF = []

            for i in np.arange(sGraphCortex.N):
                lst_RF.append (seeRFGabor(center = sGraphCortex.coords[i,:], \
                                          radius = sGraphCortex.vtxRfRad[i], \
                                          inVtxIdx = sGraphCortex.RfInVtxIdx[i], \
                                          inVtxDist = sGraphCortex.RfInVtxDist[i], \
                                          theta = thetas[j], \
                                          inGraph = sGraphRetina
                                          ))


                print i



            #add the RF list (or layer) to sGraphCortex
            sGraphCortex.addRfLayer(lst_RF)

        #create a dictionary of signal types and the corresponding index in sCortexSignals
        cortexSigTypes = {"bw-on":0, \
                          "rg-on":1, \
                          "gr-on":2, \
                          "yb-on":3, \
                          "by-on":4, \
                          "o-0": 5, \
                          "o-45": 6, \
                          "o-90": 7, \
                          "o-135": 8}

        #add it to sGraphCortex
        sGraphCortex.addSigTypes(cortexSigTypes)

 
        ###fileOp:
        pickler.dump(sGraphRetina)
        pickler.dump(sGraphCortex) 
        f_seeSystem.close()
        ###

        print "!!! seeSystem is successfully built. !!!"


    def load(self):
        """
           this function only load the pickled system object into member variables


        """

        print "!!! Loading seeSystem... !!!"

        ###get pickled objects
        f_seeSystem = open(os.path.join(self.dataPath, "seeSystem"),'rb' )

        unpickler = cPickle.Unpickler(f_seeSystem)
        self.sGraphRetina = unpickler.load()
        self.sGraphCortex = unpickler.load()
        f_seeSystem.close()
    
        print "!!! seeSystem is successfully loaded. !!!"

        ###get pickled objects


    def run(self, imgPath):

        #load the sSystem
        self.load()

        sImage = seeImage(path = imgPath, diagAng = 30)

        
        self.sGraphRetina.sampleImage(sImage,focusCenter = [0,0])
        #self.sGraphRetina.drawSignal(show = False)

        

        self.sGraphCortex.captureSignal()

        #self.sGraphCortex.draw(drawInGraph = False, drawRF = False, show = False)
        #self.sGraphCortex.drawSignal(sigIdx = self.sGraphCortex.sigTypes["by-on"] , show = False)
        #plt.show()
        sSaliency = seeSaliency(sImage, self.sGraphCortex, fixCount = 50 , inhibDiam = 1)
        sSaliency.getGlobalConspicuityMap()



        sImage.drawImage(show = False)
        plt.show()




